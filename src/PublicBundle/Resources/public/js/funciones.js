/* Video */
(function() {
    
    var video = document.getElementById("my-video");
    video.addEventListener("canplay", function () {
        video.play();
    });
    
})();

/* Carrusel */
(function($) {
    
    $(function() {
        
        var jcarousel = $('.jcarousel');

        jcarousel.on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                    
                if (width >= 1000) {
                    width = width / 5;
                } else if (width >= 920) {
                    width = width / 4;
                } else if (width >= 750) {
                    width = width / 3;
                } else if (width >= 450) {
                    width = width / 2;
                } 
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
            
        $('.jcarousel').jcarouselAutoscroll({
            target: '+=1'
        });
        
        $('.jcarousel').jcarouselAutoscroll({
            interval: 2500
        });
        
        $('.jcarousel').jcarouselAutoscroll({
            autostart: true
        });
        
        $('.jcarousel').hover(function() {
            $(this).jcarouselAutoscroll('stop');
        }, function() {
            $(this).jcarouselAutoscroll('start');
        });
        
    });
    
    
     $(function() {
        
        var jcarousel = $('.jcarousel-team');

        jcarousel.on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                    
                if (width >= 1000) {
                    width = width / 5;
                } else if (width >= 920) {
                    width = width / 4;
                } else if (width >= 750) {
                    width = width / 3;
                } else if (width >= 450) {
                    width = width / 2;
                } 
                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
            
        $('.jcarousel').jcarouselAutoscroll({
            target: '+=1'
        });
        
        $('.jcarousel').jcarouselAutoscroll({
            interval: 2500
        });
        
        $('.jcarousel').jcarouselAutoscroll({
            autostart: true
        });
        
        $('.jcarousel').hover(function() {
            $(this).jcarouselAutoscroll('stop');
        }, function() {
            $(this).jcarouselAutoscroll('start');
        });
        
    });
    
    
    
})(jQuery);


/* Menú móvil */

$( document ).ready(function() {

    //Des/aparición del menú de navegación
    $(".buttonNavmobile").unbind("click").bind("click", function(){
        
       if ($("#navmobile").css("display") == "none") {
            $('#navmobile').css({"display" : "initial"});
            $('body').css({"overflow-y" : "hidden"});
            rotateSubelement ('.buttonNavmobile', 'i', 90);
            openNav('navmobile' , '100%');
        } else {
            rotateSubelement ('.buttonNavmobile', 'i', 0);
            closeNav('navmobile', '0%');
            $('#navmobile').removeAttr("style");
            $('.buttonNavmobile').removeAttr("style");
            $('body').removeAttr("style");
        } 
    }); 
    
    //Des/aparición del submenú de navegación
    $("#certificates").unbind("click").bind("click", function(){
        
       if ($("#subdropmenuCert").css("display") == "none") {
            $("#certificates").css({"display" : "block"});
            $("#subdropmenuCert").css({"display" : "block"});
            openNav('subdropmenuCert', '200px');
        } else {
            $('#subdropmenuCert').removeAttr("style");
            $('#certificates').removeAttr("style");
        }      
    }); 
});


function openNav(elemId, elemHeight) {
    document.getElementById(elemId).style.height = elemHeight;
}

function closeNav(elemId, elemHeight) {
    document.getElementById(elemId).style.height = elemHeight;
}

/* Función para rotar sub-elementos */
function rotateSubelement (element, subelement, rotation){
    $( element + ' ' + subelement).animate({  borderSpacing: rotation }, {
        step: function(now,fx) {
            $(this).css('-webkit-transform','rotate('+now+'deg)'); 
            $(this).css('-moz-transform','rotate('+now+'deg)');
            $(this).css('transform','rotate('+now+'deg)');
        },
        duration:'slow'
    },'linear');
}