<?php

namespace PublicBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PublicController extends Controller
{

    public function indexAction(Request $request)
    {
         return $this->render('PublicBundle::pages/index.html.twig');
    }
}


